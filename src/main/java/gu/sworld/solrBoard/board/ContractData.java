package gu.sworld.solrBoard.board;

public class ContractData {
	
	  private String title;
	  private String  author;
	  private String url;
	  private String title_str;
	  private String id;
	  private String document;
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the title_str
	 */
	public String getTitle_str() {
		return title_str;
	}
	/**
	 * @param title_str the title_str to set
	 */
	public void setTitle_str(String title_str) {
		this.title_str = title_str;
	}
	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * @param author the author to set
	 */
	public void setAuthor(String author) {
		this.author = author;
	}

}
