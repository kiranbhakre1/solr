<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>board3</title>
<script>
function fn_formSubmit(){
    document.form1.submit();    
}
</script>
</head>
<body>
    <a href="board3Form">New Document insert</a>
                    
    <table border="1" style="width:600px">
        <caption>List of Document</caption>
        <colgroup>
            <col width='8%' />
            <col width='*%' />
            <col width='15%' />
            <col width='15%' />
        </colgroup>
        <thead>
            <tr>
                 <th>number</th> 
                <th>title</th>
                <th>writer</th> 
                <th>date</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="listview" items="${listview}" varStatus="status">    
                <c:url var="link" value="board3Read">
                    <c:param name="brdno" value="${listview.id}" /> 
                </c:url>                                                                  
                <tr>
                    <td><c:out value="${pageVO.totRow-((pageVO.page-1)*pageVO.displayRowCount + status.index)}"/></td>
                    <td><a href="${link}"><c:out value="${listview.author[0]}"/></a></td>
                    <td><c:out value="${listview.title_str[0]}"/></td>
                    <td><fmt:formatDate pattern = "yyyy-MM-dd" value = "${listview.brddate[0]}" /></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
    <form id="form1" name="form1"  method="post">
        <jsp:include page="/WEB-INF/views/common/pagingforSubmit.jsp" />
        
        <div>
            <input type="checkbox" name="searchType" value="title" <c:if test="${fn:indexOf(searchVO.searchType, 'title')!=-1}">checked="checked"</c:if>/>
            <label class="chkselect" for="searchType1">Search Title</label>
            <input type="checkbox" name="searchType" value="url" <c:if test="${fn:indexOf(searchVO.searchType, 'url')!=-1}">checked="checked"</c:if>/>
            <label class="chkselect" for="searchType2"> Search writer</label>
            <input type="text" name="searchKeyword" style="width:150px;" maxlength="50" value='<c:out value="${searchVO.searchKeyword}"/>' onkeydown="if(event.keyCode == 13) { fn_formSubmit();}">
            <input name="btn_search" value="search" class="btn_sch" type="button" onclick="fn_formSubmit()" />
        </div>
    </form>            
</body>
</html>
