<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>board1</title>
</head>
<body>
        <table border="1" style="width:600px">
            <caption>Board</caption>
            <colgroup>
                <col width='15%' />
                <col width='*%' />
            </colgroup>
            <tbody>
                <tr>
                    <td>writer</td> 
                    <td><c:out value="${boardInfo.brdwriter[0]}"/></td> 
                </tr>
                <tr>
                    <td>title</td> 
                    <td><c:out value="${boardInfo.brdtitle[0]}"/></td> 
                </tr>
                <tr>
                    <td>memo</td> 
                    <td><c:out value="${boardInfo.brdmemo[0]}"/></td> 
                </tr>
            </tbody> 
        </table>    
        <a href="#" onclick="history.back(-1)">Back</a>
        <a href="board1Delete?brdno=<c:out value="${boardInfo.id}"/>">delete</a>
        <a href="board1Form?brdno=<c:out value="${boardInfo.id}"/>">Edit</a>
</body>
</html>
